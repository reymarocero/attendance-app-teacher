import { NavigationContainer } from '@react-navigation/native';
import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import Login from './src/screens/Login';

import { NativeWindStyleSheet } from "nativewind";
import Dashboard from './src/screens/Dashboard';
import NavHeader from './src/components/NavHeader';
import StudentList from './src/components/StudentList';
import ScanQR from './src/screens/ScanQR';
import Reports from './src/screens/Reports';
import ReportsDetails from './src/screens/ReportsDetails';

NativeWindStyleSheet.setOutput({
  default: "native"
});

const Stack = createNativeStackNavigator();

export default function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Login">
        <Stack.Screen
          name="Login"
          component={Login}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name="Dashboard"
          component={Dashboard}
          options={{
            header: () => <NavHeader title="Schedules" disableBack={true} />,
          }}
        />
        <Stack.Screen
          name="StudentList"
          component={StudentList}
          options={{
            header: () => <NavHeader title="Student List" />,
          }}
        />
        <Stack.Screen
          name="ScanQR"
          component={ScanQR}
          options={{
            header: () => <NavHeader title="Checking Attendance" />,
          }}
        />
        <Stack.Screen
          name="Reports"
          component={Reports}
          options={{
            header: () => <NavHeader title="Reports" />,
          }}
        />
        <Stack.Screen
          name="ReportDetails"
          component={ReportsDetails}
          options={{
            header: () => <NavHeader title="Report Details" />,
          }}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
}
