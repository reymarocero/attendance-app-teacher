export const months = () => {
    const monthsData = [
      { value: 1, label: "January", abrr: "Jan" },
      { value: 2, label: "February", abrr: "Feb" },
      { value: 3, label: "March", abrr: "Mar" },
      { value: 4, label: "April", abrr: "Apr" },
      { value: 5, label: "May", abrr: "May" },
      { value: 6, label: "June", abrr: "Jun" },
      { value: 7, label: "July", abrr: "Jul" },
      { value: 8, label: "August", abrr: "Aug" },
      { value: 9, label: "September", abrr: "Sept" },
      { value: 10, label: "October", abrr: "Oct" },
      { value: 11, label: "November", abrr: "Nov" },
      { value: 12, label: "December", abrr: "Dec" }
    ];

    return monthsData;
}

export const getAbbr = (month) => {
    const months2 = months();

    const res = months2.filter(data => data.value == month);

    return res;
}