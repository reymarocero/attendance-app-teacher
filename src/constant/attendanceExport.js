export const attendanceExport = (_data, month, level, section, teacher) => {
    const data = _data;
    const daysOfWeek = ["SU", "M", "T", "W", "TH", "F", "S"];
    const header = `<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <script src="https://code.jquery.com/jquery-3.6.3.slim.min.js"
        integrity="sha256-ZwqZIVdD3iXNyGHbSYdsmWP//UBokj2FHAxKuSBKDSo=" crossorigin="anonymous"></script>
</head>
<body>
    <style>
        body {
            font-family: 'Poppins', sans-serif;
            font-size: 8px;
        }
        table td {
            border: 1px solid;
            font-size: 8px;
        }
        table {
            border-collapse: collapse;
        }
        .attendanceList td{
            width: 100px;
            height: 15px;
        }
        .attendanceDetails td {
            width: 15px;
            height: 15px;
            vertical-align: middle;
            text-align: center;
        }
        .attendanceDetails tr:first-child td {
            font-weight: bold;
            font-size: 8px;
        }
        .attendanceDetails tr:nth-child(2) td {
            font-size: 8px;
            color: #818181;
        }
        .present {
            color: blue;
            font-size: 8px;
        }
        .absent {
            color: red;
            font-size: 8px;
        }
        h1 {
            font-size: 14px;
            font-weight: 900;
        }
    </style>
    <div style="margin-bottom: 20px;">
        <h1 style="display: block; text-align: center; margin-bottom: 10px;">Daily Attendance Report of Learners</h1>
        <span style="display: block; text-align: center;">Report for the month of ${month}</span>
    </div>`;

    let ret = ``;

    ret += header;

    let thtml = ``;
    let thtml2 = ``;
    
    ret += `<div style="display: flex; margin-bottom: 10px;">
                    <span style="margin-right: 30px;">Grade Level: ${level}</span>
                    <span style="margin-right: 30px;">Section: ${section}</span>
                    <span style="margin-right: 30px;">Teacher: ${teacher}</span>
                </div>`;
    let attendanceListHead = `
                <div style="display: flex;">
                    <table class="attendanceList">
                        <tr>
                    <td style="text-align: right; padding-right: 10px;">
                        ${month}
                    </td>
                </tr>
                <tr>
                    <td style="text-align: right; padding-right: 10px;">
                        Day
                    </td>
                </tr>
            `;
    let attendanceListNames = ``;
    data.map((item) => {
      attendanceListHead += `
                <tr>
                    <td style="padding-left: 10px;">
                        ${item.fullname}
                    </td>
                </tr>
            `;
    });

    let detailsDaysHeader = `
                <table class="attendanceDetails">
            `;

    let attendanceListFooter = `</table></div>`;

    // table 1 names and months and days
    thtml += attendanceListHead;
    thtml += attendanceListNames;
    thtml += attendanceListFooter;

    // days and attendance status
    let daysBody = ``;
    let dayOfWeekBody = ``;
    let numberDays = `<tr>`;
    let htmlDayOfWeek = `<tr>`;

    let dateN = "";
    let dayName = "";

    // loop to get the days count
    for (let i = 1; i < 31 + 1; i++) {
      numberDays += `
                <td>
                    ${i}
                </td>
                `;

      dateN = new Date(`1/${i}/2023`);
      dayName = daysOfWeek[dateN.getDay()];

      htmlDayOfWeek += `<td>${dayName}</td>`;
    }

    numberDays += `<td style="padding: 0px 20px;" colspan="2">Total</td></tr>`;
    htmlDayOfWeek += `<td><span style="color: #000; font-weight: 900;">A</span></td><td><span style="color: #000; font-weight: 900;">T</span></td></tr>`;

    let detailsDaysFooter = `
                </table>
            `;

    daysBody += numberDays;
    dayOfWeekBody += htmlDayOfWeek;

    let statusBody = ``;
    let statusBodyDetails = ``;

    // data for statuses
    let absent = 0;
    let tarday = 0;
    data.map((item1) => {
      absent = 0;
      tarday = 0;
      statusBodyDetails += `<tr>`;
      for (let i = 1; i < 31 + 1; i++) {
        statusBodyDetails += `<td>`;
        item1.attendance.map((item2) => {
          if (item2.day === i) {
            statusBodyDetails += `${
              item2.status === "P"
                ? "<span class='present'>P</span>"
                : "<span class='absent'>A</span>"
            }`;

            // count the absent
            absent += item2.status === "A" ? 1 : 0;
            tarday += item2.status === "T" ? 1 : 0;
          }
        });
        statusBodyDetails += `</td>`;
      }
      statusBodyDetails += `<td>${absent}</td><td>${tarday}</td></tr>`;
    });

    statusBody += statusBodyDetails;

    // for table2 attendance status and details
    thtml2 += detailsDaysHeader;
    thtml2 += daysBody;
    thtml2 += dayOfWeekBody;
    thtml2 += statusBody;
    thtml2 += detailsDaysFooter;

    // combine to table
    ret += `<div style="display: flex; justify-content: center;">`;
    ret += `<div>${thtml}</div>`;
    ret += `<div>${thtml2}</div></div>`;

    const footer = `</body></html>`;

    ret += footer;

    return ret;
}