import { useEffect, useState } from "react";
import { View, Text, FlatList, TouchableOpacity } from "react-native";
import { supabase } from "../lib/supabase";
import Ionicons from "@expo/vector-icons/Ionicons";
import dateFormat from "dateformat";

const StudentList = ({ navigation, route }) => {
  const {
    scheduler_id,
    teacher_id,
    section_name,
    level_code,
    level_name,
    subject_name,
    end_time,
    start_time } = route.params;
  
    const [students, setStudents] = useState([]);

  const getStudents = async () => {
    const dateNow = new Date();

    const { data, error } = await supabase
      .from("enrolles")
      .select(
        `id, fullname, attendance(teacher_id,id, created_at, month, day, year,status)`
      )
      .eq("year_level", level_code)
      .eq("section_name", section_name)
      .eq("attendance.teacher_id", teacher_id)
      .eq("attendance.day", dateNow.getDate())
      .eq("attendance.month", dateNow.getMonth() + 1)
      .eq("attendance.year", dateNow.getFullYear())
        .order("fullname", { ascending: true });
        
        if (error) {
            console.error("Error on retrieving students", error);
            return;
        }

    setStudents(data);
  }

  const renderStudents = ({ item }) => {
    const dateNow = new Date();

    const getDay = dateNow.getDate();
    const getMonth = dateNow.getMonth() + 1;
  
      return (
        <TouchableOpacity key={item.id}>
          <View className="py-2 border-b border-gray-100">
            <View className="flex flex-row justify-between">
              <Text className="text-[#525252] text-[16px]">
                {item.fullname}
              </Text>
              <View className="pr-5">
                {item.attendance.map((item2) => (
                  <View key={item2.id}>
                    {item2.day === getDay &&
                    item2.month === getMonth &&
                    item2.status === "P" ? (
                      <View className="w-[20px] h-[20px] rounded-full bg-[#E6FFD2] flex items-center justify-center border border-green-600">
                        <Ionicons
                          name="checkmark-outline"
                          size={15}
                          color="#2BA495"
                        />
                      </View>
                    ) : (
                      <View className="w-[20px] h-[20px] rounded-full bg-red-100 flex items-center justify-center border border-red-600">
                        <Text className="text-red-500">A</Text>
                      </View>
                    )}
                  </View>
                ))}
              </View>
            </View>
          </View>
        </TouchableOpacity>
      );
    }

    useEffect(() => {
        const init = async () => {
            await getStudents();
        }

      init();
    },[])

    return (
      <View>
        <View className="p-5 bg-white border-b border-gray-200">
          <View className="flex flex-row justify-between">
            <View className="grid gap-y-2">
              <Text className="text-[#1E8CE1] font-bold text-[24px]">
                {level_name}
              </Text>
              <View className="flex flex-row gap-x-2">
                <Text className="font-bold text-[#909090] text-[14px] uppercase">
                  {subject_name}
                </Text>
                <Text className="font-bold text-[#909090] text-[14px] uppercase">
                  {section_name}
                </Text>
              </View>
            </View>
            <View className="grid gap-y-1">
              <Text className="font-bold text-[#909090] text-[14px]">
                Schedule
              </Text>
              <Text className="font-bold text-[15px]">
                {dateFormat(start_time, "shortTime")} -{" "}
                {dateFormat(end_time, "shortTime")}
              </Text>
            </View>
          </View>
        </View>
        <View className="p-5 bg-white">
          <View className="flex flex-row justify-between mb-3">
            <Text className="text-[16px] font-bold">Name of Learners</Text>
            <Text className="text-[16px] font-bold">Attendance Status</Text>
          </View>
          <FlatList
            data={students}
            renderItem={renderStudents}
            className="relative"
          />
          <View className="mt-5 flex flex-row gap-x-2 justify-between items-center">
            <Text className="font-bold">{students.length} Students</Text>
            <TouchableOpacity
              className="p-4 bg-[#3290D3] rounded-md"
              onPress={() =>
                navigation.navigate("ScanQR", {
                  scheduler_id: scheduler_id,
                  teacher_id: teacher_id,
                  level_code: level_code,
                  section_name: section_name,
                })
              }
            >
              <Text className="text-white text-center font-bold">
                Check attendance
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
}

export default StudentList;