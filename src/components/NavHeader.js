import { View, Text, TouchableOpacity, SafeAreaView } from "react-native";
import { useNavigation } from "@react-navigation/native";
import Ionicons from "@expo/vector-icons/Ionicons";

const NavHeader = (props) => {
  const navigation = useNavigation();
  return (
    <SafeAreaView className="bg-[#D2ECFF]">
      <View className="flex flex-row justify-between items-center px-2 pt-10 pb-2">
        <View className="flex-shrink-0">
          {!props.disableBack && (
            <TouchableOpacity onPress={() => navigation.goBack()}>
              <View className="flex flex-row gap-3 items-center">
                <Ionicons name="md-chevron-back-outline" size={32} />
                <Text className="font-bold ">{props.title}</Text>
              </View>
            </TouchableOpacity>
          )}
        </View>
        <View className="flex-shrink-0">
          <TouchableOpacity onPress={() => navigation.navigate("Login")}>
            <Text className="text-cyan-600">Log out</Text>
          </TouchableOpacity>
        </View>
      </View>
    </SafeAreaView>
  );
};

export default NavHeader;