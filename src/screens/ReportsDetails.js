import { useEffect, useState } from "react";
import { View, Text, FlatList, ScrollView, TouchableOpacity } from "react-native";
import { SelectList } from "react-native-dropdown-select-list";
import { supabase } from "../lib/supabase.js";
import dateFormat from "dateformat";
import { getAbbr } from "../constant/monthPerYear.js";
import Ionicons from "@expo/vector-icons/Ionicons";
import { attendanceExport } from "../constant/attendanceExport.js";

import * as Print from "expo-print";
import * as Sharing from "expo-sharing";
import * as MediaLibrary from "expo-media-library";
import * as FileSystem from "expo-file-system";
import { Alert } from "../components/Alert.js";

export default function ReportsDetails({ navigation, route }) {
  const { teacher_id, subject_name, level_code, section_name, month, level_name, end_time, start_time } = route.params;

  const monthAbrr = getAbbr(month);

  const [attendance, setAttendance] = useState([]);
  const [days, setDays] = useState([]);
  const [success, setSuccess] = useState(false);

  const _thtml = attendanceExport(attendance, monthAbrr[0].label, level_name, section_name, teacher_id);
  
  const getReportData = async () => {
    const { data, error } = await supabase
      .from("enrolles")
      .select(
        `id, fullname, attendance(teacher_id,id, created_at, month, day,status)`
      )
      .eq("year_level", level_code)
      .eq("section_name", section_name)
      .eq("attendance.month", month)
      .eq("attendance.teacher_id", teacher_id)
      .order("fullname", {ascending: true});
    
    if (error) {
      console.error(error);
    }
    setAttendance(data);
  }

  const RenderAttendance = () => {
    return attendance.map((item) => (
      <View
        key={item.id}
        className="h-[27px] w-full border-b border-gray-100 flex justify-center pr-2"
      >
        <Text className="font-bold">{item.fullname}</Text>
      </View>
    ));
  }

  const RenderAttendanceDetails = () => {
    const daysOfMonth = new Date(2023, month, 0).getDate();

    return attendance.map((item) => (
      <View key={item.attendance.id}>
        <View className="flex flex-row">
          {days.map((item2) => (
            <View key={item2} className="w-[35px] h-[27px] border border-gray-100 flex items-center justify-center">
              {item.attendance.map((item3) => (
                <View key={item3.id}>
                  {item3.day === item2 && (
                    <View>
                      {item3.status === "P" ? (
                        <View className="w-[20px] h-[20px] flex items-center justify-center rounded-full bg-[#E6FFD2]">
                          <Ionicons
                            name="checkmark-outline"
                            size={15}
                            color="#2BA495"
                          />
                        </View>
                      ) : (
                        <View className="w-[20px] h-[20px] flex items-center justify-center rounded-full bg-[#FFCBBF]">
                          <Text className="text-[#FF4127]">A</Text>
                        </View>
                      )}
                    </View>
                  )}
                </View>
              ))}
            </View>
          ))}
        </View>
      </View>
    ));
  }

  const getDaysInMonth = async () => {
    const daysOfMonth = new Date(2023, month, 0).getDate();

    const newArray = [];

    for (let i = 1; i < daysOfMonth + 1; i++) {
      newArray.push(i);
    }

    setDays(newArray);
  }

  const RenderDateAttendance = () => {

    return days.map((item) => (
      <View
        key={item}
        className="w-[35px] h-[27px] border border-gray-100 border-b-0 flex items-center justify-center font-bold"
      >
        <Text>{item}</Text>
      </View>
    ));
  }

  const handleDownloadAttendance = () => {
    createAndSavePDF(_thtml);
  }

  const createAndSavePDF = async (html) => {
    try {
      const dateNow = new Date();
      const { uri } = await Print.printToFileAsync({ html });
      const newUri = `${uri.slice(0, uri.indexOf('/Print/') + 1)}Print/report_${dateNow.getTime()}.pdf`;

      if (Platform.OS === "ios") {
        await Sharing.shareAsync(uri);
      } else {
        
        const permission = await MediaLibrary.requestPermissionsAsync();

        if (permission.granted) {
          await FileSystem.moveAsync({
            from: uri,
            to: newUri,
          });

          const asset = await MediaLibrary.createAssetAsync(newUri);
          await MediaLibrary.createAlbumAsync("AttendanceApp", asset, false);
          
          setSuccess(true);
        }
      }
    } catch (error) {
      console.error(error);
    }
  };
  
  useEffect(() => {
    const init = async () => {
      await getReportData();
      await getDaysInMonth();
    }
    init();
  },[])
  return (
    <View>
      <View className="bg-white border-b border-gray-100 p-5">
        {success && (
          <Alert
            title="Download complete"
            message="You can check your report here /Picture/AttendanceApp/"
          />
        )}
        <View className="flex flex-row justify-between items-center">
          <View className="p-3 bg-[#F6F6F6] rounded-full px-10">
            <Text className="text-center text-[#8B8B8B] font-bold">
              {subject_name}
            </Text>
          </View>
          <TouchableOpacity onPress={handleDownloadAttendance}>
            <Ionicons name="cloud-download-outline" size={24} color="blue" />
          </TouchableOpacity>
        </View>
      </View>
      <View className="px-5 py-5 bg-white">
        <View className="flex flex-row justify-between">
          <View className="flex flex-row gap-x-5">
            <Text className="text-[15px] font-semibold">{level_name}</Text>
            <Text className="text-[14px] font-medium">{section_name}</Text>
          </View>
          <View>
            <Text className="text-blue-500">
              {start_time} - {end_time}
            </Text>
          </View>
        </View>
      </View>
      <View className="bg-white p-5">
        <View className="flex flex-row">
          <View className="w-6/12">
            <View className="h-[27px] border-b border-gray-100 flex flex-row items-center pr-2">
              <Text className="w-full font-bold text-right text-cyan-600">
                {monthAbrr[0].abrr}
              </Text>
            </View>
            <View>
              <RenderAttendance />
            </View>
          </View>
          <View className="w-6/12">
            <ScrollView horizontal={true} style={{ width: "100%" }}>
              <View>
                <View className="flex flex-row">
                  <RenderDateAttendance />
                </View>
                <View>
                  <RenderAttendanceDetails />
                </View>
              </View>
            </ScrollView>
          </View>
        </View>
      </View>
    </View>
  );
}