import { useEffect, useState } from 'react';
import { FlatList, Text, View, TouchableOpacity } from 'react-native';
import { supabase } from '../lib/supabase';
import dateFormat from 'dateformat';


const Dashboard = ({ navigation, route }) => {
    const [teacherScheduleToday, setTeacherScheduleToday] = useState([]);
    const date = new Date();
    const { teacher_id } = route.params;

  const scheduleNow = async () => {
    const dateNow = new Date();
    
    const { data, error } = await supabase
      .from("scheduler")
      .select(
        `id, start_time, end_time, teacher_id, what_day,section_name, year_level(level_name, level_code), subjects(subject_code, subject_name)`
      )
      .eq("what_day", dateNow.getDay())
          .eq("teacher_id", teacher_id);
        
        if (error) {
            console.error("Error on retreiving schedule", error);
            return;
        }

        setTeacherScheduleToday(data);

    }

    const renderScheduleToday = ({ item }) => {
        return (
          <TouchableOpacity
            key={item.id}
            onPress={() =>
              navigation.navigate("StudentList", {
                scheduler_id: item.id,
                section_name: item.section_name,
                level_code: item.year_level.level_code,
                teacher_id: teacher_id,
                level_name: item.year_level.level_name,
                subject_name: item.subjects.subject_name,
                start_time: item.start_time,
                end_time: item.end_time
              })
            }
          >
            <View className="p-4 rounded-lg bg-white mb-3 flex flex-row justify-between items-center">
              <View>
                <Text className="font-bold text-lg">{item.year_level.level_name}</Text>
                <Text className="font-semibold text-gray-600">{item.section_name} - {item.subjects.subject_name }</Text>
              </View>
              <View className="flex flex-row gap-x-2">
                <Text>{dateFormat(item.start_time, "shortTime")}</Text>
                <Text>-</Text>
                <Text>{dateFormat(item.end_time, "shortTime")}</Text>
              </View>
            </View>
          </TouchableOpacity>
        );
    }

    useEffect(() => {
        const init = async () => {
            await scheduleNow();
        }

        init();
    },[]);

    return (
      <View className="mt-5 mx-5">
        <View className="flex flex-row justify-between items-center mb-5">
          <View>
            <Text className="font-bold">Schedule Class</Text>
          </View>
          <TouchableOpacity
            onPress={() =>
              navigation.navigate("Reports", {
                teacher_id: teacher_id,
              })
            }
          >
            <Text className="text-cyan-500 font-bold">See reports</Text>
          </TouchableOpacity>
        </View>

        <FlatList
          data={teacherScheduleToday}
          renderItem={renderScheduleToday}
        />
      </View>
    );
}

export default Dashboard;