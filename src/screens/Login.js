import {useState } from "react";
import { Text, View, TextInput, TouchableOpacity, Alert } from "react-native";
import { supabase } from "../lib/supabase";

const Login = ({navigation}) => {
    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");

    const handleLogin = async () => {

        const { data, error } = await supabase
            .from("teacher")
            .select("*")
            .eq("username", username)
            .eq("password", password);
        
        if (error) {
            console.error("Error on login data", error);
            return;
        }

        if (data.length === 0) {
            Alert.alert("Authentication Error","Wrong Password or Username. Please try again.");
            return;
        }

        // if success navigate to dashboard
        navigation.navigate("Dashboard", {
          teacher_id: data[0].id,
        });

        
    }
    
    return (
      <View>
        <View className="bg-white h-[200px] flex justify-end items-center">
          <View>
            <Text className="mb-5 text-2xl font-bold">Welcome Teacher</Text>
          </View>
        </View>
        <View className="px-5 pt-5 bg-gray-50">
          <View className="grid gap-6">
            <View className="grid gap-y-2">
              <Text>Username</Text>
              <TextInput
                className="p-4 border border-cyan-700 rounded-md bg-white"
                onChange={(e) => setUsername(e.nativeEvent.text)}
              ></TextInput>
            </View>
            <View className="grid gap-y-2">
              <Text>Password</Text>
              <TextInput
                className="p-4 border border-cyan-700 rounded-md bg-white"
                onChange={(e) => setPassword(e.nativeEvent.text)}
                secureTextEntry={true}
              ></TextInput>
            </View>
            <View>
              <TouchableOpacity
                onPress={handleLogin}
                className="py-5 bg-cyan-500 rounded-lg"
              >
                <Text className="text-center text-white font-bold">Login</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </View>
    );
}

export default Login;