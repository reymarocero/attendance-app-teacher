import { useEffect, useState } from "react";
import { View, Text, FlatList, TouchableOpacity } from "react-native";
import { SelectList } from "react-native-dropdown-select-list";
import { supabase } from "../lib/supabase.js";
import dateFormat from "dateformat";
import { months } from "../constant/monthPerYear.js";
import Ionicons from "@expo/vector-icons/Ionicons";

import DropDownPicker from "react-native-dropdown-picker";

export default function Reports({ navigation, route }) {
  const { teacher_id } = route.params;

  const [monthsKey, setMonthsKey] = useState("");
  const [subject, setSubject] = useState("");
  const [subjectDataState, setSubjectDataState] = useState([]);
  const [schedulerData, setSchedulerData] = useState([]);
  const [handleClass, setHandleClass] = useState([]);
  const [monthsData, setMonthsData] = useState(months());

  const [open, setOpen] = useState(false);
  const [openMonth, setOpenMonth] = useState(false);
  const subjectData = [];

  const getSubjectMonth = async () => {
    const { data, error } = await supabase
      .from("subjects")
      .select(`subject_code, subject_name,scheduler(teacher_id)`)
      .eq("scheduler.teacher_id",teacher_id);
    
    if (error) {
      console.error(error);
      return;
    }

    setSchedulerData(data);
  }

  const getSubjectData = () => {
    const f = schedulerData.filter(item => item.scheduler.length > 0);
    f.map(item => {
      subjectData.push({
        value: item.subject_code,
        label: item.subject_name,
      });
    });

    setSubjectDataState(subjectData);

  }

  const getListHandled = async () => {
    const { data, error } = await supabase
      .from("scheduler")
      .select(`id, subject_code, section_name, start_time, end_time, year_level(level_code, level_name), subjects(subject_name)`)
      .eq("subject_code", subject);
    
    if (error) {
      console.error(error);
    }

    setHandleClass(data);
  }

  const renderHandleClass = ({item}) => {
    return (
      <TouchableOpacity
        key={item.id}
        className="mb-3 bg-white px-5 py-8 rounded-md"
        onPress={() =>
          navigation.navigate("ReportDetails", {
            teacher_id: teacher_id,
            subject_name: item.subjects.subject_name,
            level_code: item.year_level.level_code,
            section_name: item.section_name,
            month: monthsKey,
            level_name: item.year_level.level_name,
            start_time: dateFormat(item.start_time, "shortTime"),
            end_time: dateFormat(item.end_time, "shortTime"),
          })
        }
      >
        <View>
          <View className="flex flex-row justify-between items-center">
            <View>
              <Text className="font-semibold text-[20px]">
                {item.year_level.level_name}
              </Text>
              <Text className="font-medium text-[15px] text-[#818181]">
                {item.section_name}
              </Text>
            </View>
            <View className="flex flex-row gap-x-2">
              <Text className="font-medium text-[#1E8CE1] text-[18px]">
                {dateFormat(item.start_time, "shortTime")}
              </Text>
              <Text className="font-medium text-[#1E8CE1] text-[18px]">-</Text>
              <Text className="font-medium text-[#1E8CE1] text-[18px]">
                {dateFormat(item.end_time, "shortTime")}
              </Text>
            </View>
          </View>
        </View>
      </TouchableOpacity>
    );
  }
  
  useEffect(() => {
    const init = async () => {
      await getSubjectMonth();
    }
    init();
  },[])
    return (
      <View>
        <View className="bg-white p-5 relative z-10">
          <View className="flex flex-row items-center">
            <View className="mr-2 w-5/12">
              <DropDownPicker
                open={openMonth}
                value={monthsKey}
                items={monthsData}
                setOpen={setOpenMonth}
                setValue={setMonthsKey}
                setItems={setMonthsData}
                onChangeValue={() => getSubjectData()}
                placeholder="Month"
              />
            </View>
            <View className="w-5/12 mr-4">
              <DropDownPicker
                open={open}
                value={subject}
                items={subjectDataState}
                setOpen={setOpen}
                setValue={setSubject}
                setItems={setSubjectDataState}
                placeholder="Subject"
              />
            </View>
            <View className="w-2/12">
              <TouchableOpacity onPress={getListHandled}>
                <Ionicons name="search" size={24} color="blue" />
              </TouchableOpacity>
            </View>
          </View>
        </View>
        <FlatList
          data={handleClass}
          renderItem={renderHandleClass}
          className="mt-5 mx-5 relative z-0"
        />
      </View>
    );
}