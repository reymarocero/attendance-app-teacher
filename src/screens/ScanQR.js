import { View, Text, TouchableOpacity } from "react-native";
import { supabase } from "../lib/supabase";
import { BarCodeScanner } from "expo-barcode-scanner";
import { useState, useEffect } from "react";
import Ionicons from "@expo/vector-icons/Ionicons";
import { Alert } from "../components/Alert";

export default function ScanQR({ navigation, route }) {
  const { scheduler_id, teacher_id, section_name, level_code } = route.params;
  const [hasPermission, setHasPermission] = useState(null);
  const [scanned, setScanned] = useState(false);
  const [studentDetails, setStudentDetails] = useState([]);
  const [attendanceExist, setAttendanceExist] = useState(false);
  const [notBelong, setNotBelong] = useState(false);

  useEffect(() => {
    const getBarCodeScannerPermissions = async () => {
      const { status } = await BarCodeScanner.requestPermissionsAsync();
      setHasPermission(status === "granted");
    };

    getBarCodeScannerPermissions();
  }, []);

  const handleBarCodeScanned =  async({ type, data }) => {
      setScanned(true);
      await compareQR(data);
  };

  if (hasPermission === null) {
    return <Text>Requesting for camera permission</Text>;
  }
  if (hasPermission === false) {
    return <Text>No access to camera</Text>;
  }

  const compareQR = async (qrcode) => {
    const { data, error } = await supabase
      .from("enrolles")
      .select(
        `*,year_level(level_name)`
      )
      .eq("year_level", level_code)
      .eq("section_name", section_name)
      .eq("qrcode", qrcode);
    
    if (error) {
        console.error(error);
        return;
    }

    if (data.length == 0) {
      setNotBelong(true);
      return;
    }

    setStudentDetails(data);

    // save to db
    await handleSaveAttendance(data[0].id);
  }
  
  const handleSaveAttendance = async (enrolles_id) => {
    const dateNow = new Date();
    const { data, errorAttendance } = await supabase
      .from("attendance")
      .select("enrolles_id, day, month")
      .eq("enrolles_id", enrolles_id)
      .eq("day", dateNow.getDate())
      .eq("month", dateNow.getMonth() + 1)
      .eq("schedule_id", scheduler_id);
    
    if (errorAttendance) {
      console.error(errorAttendance);
      return;
    }

    if (data.length > 0) {
      setAttendanceExist(true);
      return;
    }

    const { error } = await supabase.from("attendance").insert({
      schedule_id: scheduler_id,
      teacher_id: teacher_id,
      enrolles_id: enrolles_id,
      day: dateNow.getDate(),
      month: dateNow.getMonth() + 1,
      year: dateNow.getFullYear(),
      status: "P",
    });
    
    if (error) {
      console.error(error);
      return;
    }

    console.log("Sucess");
  }

    return (
      <View className="mx-5 mt-5">
        <View className="h-screen items-center">
          <View className="w-full p-4 rounded-lg">
            <View className="flex items-center">
              {studentDetails &&
                studentDetails.map((item) => {
                  return (
                    <View
                      className="my-8 grid gap-y-2 items-center"
                      key={item.id}
                    >
                      <View className="w-[50px] h-[50px] rounded-full bg-[#E6FFD2] flex items-center justify-center">
                        <Ionicons
                          name="checkmark-outline"
                          size={24}
                          color="#2BA495"
                        />
                      </View>
                      <Text className="font-bold text-[20px] text-[#818181]">
                        {item.fullname}
                      </Text>
                      <View className="flex flex-row gap-x-2">
                        <Text className="text-[16px] text-[#818181]">
                          {item.year_level.level_name}
                        </Text>
                        <Text className="text-[16px] text-[#818181]">
                          {item.section_name}
                        </Text>
                      </View>
                    </View>
                  );
                })}
              {attendanceExist && (
                <Alert title="Error" message="This errolles id has been checked the attendance already. Please scan again." type="error" />
              )}
              {notBelong && (
                <Alert title="Error" message="Student QR doest not exist or not belong in your class schedule. Please try again." type="error" />
              )}
            </View>
            <BarCodeScanner
              onBarCodeScanned={scanned ? undefined : handleBarCodeScanned}
              className="w-full h-[400px]"
            />
            {scanned && (
              <TouchableOpacity
                className="mt-4 p-3 bg-blue-600"
                onPress={() => {
                  setScanned(false);
                  setStudentDetails([]);
                  setNotBelong(false);
                  setAttendanceExist(false);
                }}
              >
                <Text className="text-center text-white font-bold rounded-md">
                  Scan Again
                </Text>
              </TouchableOpacity>
            )}
          </View>
        </View>
      </View>
    );
}